﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ScopeApp
{
    /// <summary>
    /// Näytejono.
    /// Jono on turvallinen käyttää monisäikeisessä ympäristössä ilman lukkoja kun varmistetaan että kerrallaan vain yksi lukija ja/tai yksi kirjoittaja.
    /// </summary>
    public class ScopeQueue
    {
        readonly float[] queue;
        readonly int capacity;
        readonly int capacityMask;
        int head;
        int tail;
        int length;

        public int Capacity
        {
            get { return capacity; }
        }

        public int Length
        {
            get { return Interlocked.CompareExchange( ref length, 0, 0 ); }
        }

        public ScopeQueue( int capacity )
        {
            if ( !ScopeMath.IsPowerOfTwo( capacity ) )
                throw new ArgumentOutOfRangeException( "Kapasiteetin täytyy olla kahden potenssi." );

            this.queue = new float[ capacity ];
            this.capacity = capacity;
            this.capacityMask = ~(capacity);
            this.head = 0;
            this.tail = 0;
            this.length = 0;
        }

        public float Read()
        {
            var value = queue[ head ];
            head = (head + 1) & capacityMask;
            Interlocked.Add( ref this.length, -1 );
            return value;
        }

        public void Write( float value )
        {
            queue[ tail ] = value;
            tail = (tail + 1) & capacityMask;
            Interlocked.Add( ref this.length, 1 );
        }

        public unsafe void Peek( float[] destination, int length )
        {
            if ( this.length < length )
                throw new ArgumentException();

            var peekHead = head;

            for ( var n = 0; n < length; n++ )
            {
                destination[ n ] = queue[ peekHead ];
                peekHead = (peekHead + 1) & capacityMask;
            }
        }

        public void Read( int amount )
        {
            for ( var n = 0; n < amount; n++ )
            {
                queue[ head ] = 0f;
                head = (head + 1) & capacityMask;
            }

            Interlocked.Add( ref this.length, -amount );
        }

        public void Transfer( int length, ScopeBuffer buffer )
        {
            if ( this.length < length )
                throw new ArgumentException();

            var dest = buffer.UnsafeGetBuffer();
            var destHead = buffer.UnsafeGetWriteHead();
            var destHeadMask = ~buffer.Capacity;

            for ( var n = 0; n < length; n++ )
            {
                dest[ destHead ] = queue[ head ];
                destHead = (destHead + 1) & destHeadMask;

                queue[ head ] = 0f;
                head = (head + 1) & capacityMask;
            }

            buffer.UnsafeSetWriteHead( destHead );

            Interlocked.Add( ref this.length, -length );
        }

        public void Read( float[] destination, int length )
        {
            if ( this.length < length )
                throw new ArgumentException();

            for ( var n = 0; n < length; n++ )
            {
                destination[ n ] = queue[ head ];
                queue[ head ] = 0f;
                head = (head + 1) & capacityMask;
            }

            Interlocked.Add( ref this.length, -length );
        }

        public void Write( float[] source, int length )
        {
            for ( var n = 0; n < length; n++ )
            {
                queue[ tail ] = source[ n ];
                tail = (tail + 1) & capacityMask;
            }

            Interlocked.Add( ref this.length, length );
        }
    }
}

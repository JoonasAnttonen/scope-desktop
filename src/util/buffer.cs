﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScopeApp
{
    /// <summary>
    /// Näyterinkipuskuri.
    /// Ei ole turvallinen monisäikeisessä ympäristössä ilman lukkoja.
    /// </summary>
    public class ScopeBuffer
    {
        readonly float[] buffer;
        readonly int capacity;
        readonly int capacityMask;
        int head;

        /// <summary> Näytepuskurin kapasitetti. </summary>
        public int Capacity
        {
            get { return capacity; }
        }

        /// <summary> Luo uuden näytepuskurin. </summary>
        /// <param name="capacity">Haluttu kapasiteetti.</param>
        public ScopeBuffer( int capacity )
        {
            if ( !ScopeMath.IsPowerOfTwo( capacity ) )
                throw new ArgumentOutOfRangeException( "Kapasiteetin täytyy olla kahden potenssi." );

            this.buffer = new float[ capacity ];
            this.head = 0;
            this.capacity = capacity;
            this.capacityMask = ~(capacity);
        }

        /// <summary> TARKKANA! Suora pääsy näytekokoelmaan. </summary>
        public float[] UnsafeGetBuffer()
        {
            return buffer;
        }

        /// <summary> TARKKANA! Palauttaa näytekokoelman kirjoituspään. </summary>
        public int UnsafeGetWriteHead()
        {
            return head;
        }

        /// <summary> TARKKANA! Asettaa näytekokoelman kirjoituspään. </summary>
        public void UnsafeSetWriteHead( int position )
        {
            head = position;
        }

        /// <summary>  Lukee näytteen halutusta kohdasta.  </summary>
        /// <param name="offset">Kohta.</param>
        public float Read( int offset )
        {
            return buffer[ (head + offset) & capacityMask ];
        }

        /// <summary> Kirjoittaa näytteen puskuriin. </summary>
        /// <param name="value">Näyte.</param>
        public void Write( float value )
        {
            buffer[ head ] = value;
            head = (head + 1) & capacityMask;
        }

        /// <summary> Tyhjentää puskurin. </summary>
        public void Clear()
        {
            for ( var n = 0; n < capacity; n++ )
                buffer[ n ] = 0f;

            head = 0;
        }

        /// <summary> Kirjoittaa annetut näytteet puskuriin. </summary>
        /// <param name="values">Näytteet.</param>
        /// <param name="start">Näytteiden aloitus indeksi.</param>
        /// <param name="length">Näytteiden määrä.</param>
        public unsafe void Write( float[] values, int start, int length )
        {
            fixed ( float* fixedBuffer = buffer )
            fixed ( float* fixedValues = values )
            {
                for ( var n = start; n < (start + length); n++ )
                {
                    fixedBuffer[ head ] = fixedValues[ n ];
                    head = (head + 1) & capacityMask;
                }
            }
        }
    }
}

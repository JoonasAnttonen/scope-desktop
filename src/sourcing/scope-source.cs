﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using System.IO.Ports;

namespace ScopeApp
{
    /// <summary> Lähteen tulon tila. </summary>
    public enum ScopeCoupling
    {
        /// <summary> DC komponentti mukana. </summary>
        DC = 0,
        /// <summary> DC komponentti suodatettu pois. </summary>
        AC = 1,
        /// <summary> GND. </summary>
        GND = 2
    }

    /// <summary>
    /// Abstraktio näytelähteille.
    /// </summary>
    public abstract class ScopeSource
    {
        /// <summary>Tyhjän lähteen määritelmä. </summary>
        class ScopeSourceNull : ScopeSource
        {
            public override float Samplerate
            {
                get { return 0.000001f; }
            }
        }

        /// <summary> Tyhjä lähde. </summary>
        public static readonly ScopeSource Empty = new ScopeSourceNull();

        protected readonly AutoResetEvent stopEvent = new AutoResetEvent( false );
        protected readonly ScopeQueue input = new ScopeQueue( 65536 );

        /// <summary> Lähteen näytenopeus. </summary>
        public abstract float Samplerate { get; }

        /// <summary> Tarjolla olevien näytteiden määrän. </summary>
        public int AvailableSamples
        {
            get { return input.Length; }
        }

        /// <summary> Onko lähde yhdistetty? </summary>
        public virtual bool Connected
        {
            get { return true; }
        }

        /// <summary> Onko lähde synkronoitu? </summary>
        public virtual bool Synchronized
        {
            get { return true; }
        }

        /// <summary> Lähteen tulon tila. </summary>
        public virtual ScopeCoupling Coupling
        {
            get { return ScopeCoupling.DC; }
            set { }
        }

        public ScopeSource()
        {
        }

        protected virtual void Run( object state )
        {
            while ( true )
            {
                lock ( input )
                {
                    while ( input.Length < input.Capacity )
                        input.Write( 0.0f );
                }
            }
        }

        void WaitForSamples( int samples )
        {
            while ( input.Length < samples ) ;
        }

        /// <summary> Käynnistää lähteen tausta säikeen. </summary>
        public void Start()
        {
            ThreadPool.QueueUserWorkItem( new WaitCallback( Run ) );
        }

        /// <summary> Pysäyttää lähteen tausta säikeen jonkin lyhyen mutta määrittelemättön ajan kuluttua. </summary>
        public void Stop()
        {
            stopEvent.Set();
        }

        /// <summary> 
        /// Siirtää annetun määrän näytteitä lähteestä annettuun puskuriin.
        /// Jos näitteitä ei ole tarpeeksi, odottaa että tarpeeksi saapuu.
        /// </summary>
        /// <param name="amount">Näytteiden määrä.</param>
        /// <param name="buffer">Puskurin johon näytteet siirretään.</param>
        public void Transfer( int amount, ScopeBuffer buffer )
        {
            if ( input.Length < amount )
                WaitForSamples( amount );

            input.Transfer( amount, buffer );
        }

        /// <summary>  Poistaa annetun määrän näytteitä puskurista. </summary>
        public void Clear()
        {
            input.Read( input.Length );
        }

        /// <summary>  Poistaa annetun määrän näytteitä puskurista. </summary>
        /// <param name="amount">Poistettava määrä.</param>
        public bool Clear( int amount )
        {
            if ( input.Length >= amount )
            {
                input.Read( amount );
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary> 
        /// Lukee annetun määrän näytteitä puskurista.
        /// Jos näitteitä ei ole tarpeeksi, odottaa että tarpeeksi saapuu.
        /// </summary>
        /// <param name="buffer">Puskuri johon lukea.</param>
        /// <param name="amount">Näytteiden määrä.</param>
        public void Read( float[] buffer, int amount )
        {
            if ( input.Length < amount )
                WaitForSamples( amount );

            input.Read( buffer, amount );
        }

        /// <summary> 
        /// Lukee annetun määrän näytteitä puskurista poistamatta niitä.
        /// Jos näitteitä ei ole tarpeeksi, odottaa että tarpeeksi saapuu.
        /// </summary>
        /// <param name="buffer">Puskuri johon lukea.</param>
        /// <param name="amount">Näytteiden määrä.</param>
        public void Peek( float[] buffer, int amount )
        {
            if ( input.Length < amount )
                WaitForSamples( amount );

            input.Peek( buffer, amount );
        }
    }
}

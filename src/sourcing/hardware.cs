﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using System.IO.Ports;

namespace ScopeApp
{
    /// <summary>
    /// Laitteisto lähde.
    /// </summary>
    public class HardwareSource : ScopeSource
    {
        enum PacketType
        {
            None,
            Payload = 5000,
            GetControl = 5001,
            SetControl = 5002
        }

        static readonly AutoResetEvent controlEvent = new AutoResetEvent( false );

        readonly byte[] synchBuffer = new byte[ 2 ];
        string devicePort;
        int isConnected = 0;
        int isSynchronized = 0;
        int coupling = 0;

        public override bool Connected
        {
            get { return (Interlocked.CompareExchange( ref isConnected, 0, 0 ) == 1); }
        }

        public override bool Synchronized
        {
            get { return (Connected && (Interlocked.CompareExchange( ref isSynchronized, 0, 0 ) == 1)); }
        }

        public override ScopeCoupling Coupling
        {
            get { return (ScopeCoupling)Interlocked.CompareExchange( ref coupling, 0, 0 ); }
            set
            {
                Interlocked.Exchange( ref coupling, (int)value );
                controlEvent.Set();
            }
        }

        /// <summary> Lähteen näytteenottonopeus. </summary>
        public override float Samplerate
        {
            get { return (1f / 16666.666f); }
        }

        /// <summary> Avoinna olevat portit. </summary>
        public static string[] AvailablePorts
        {
            get { return SerialPort.GetPortNames(); }
        }

        /// <summary> Luo uuden laitteistolähteen. </summary>
        /// <param name="devicePort">Portti johon yhdistetään.</param>
        public HardwareSource( string devicePort )
        {
            this.devicePort = devicePort;
        }

        void Connect( SerialPort port )
        {
            try
            {
                port.Close();

                if ( String.IsNullOrEmpty( AvailablePorts.FirstOrDefault( ( p ) => p == devicePort ) ) )
                {
                    devicePort = AvailablePorts.FirstOrDefault();
                }

                if ( String.IsNullOrEmpty( devicePort ) )
                    devicePort = "COM1";

                port.PortName = devicePort;
                port.BaudRate = 750000;
                port.ReadTimeout = 2000;
                port.WriteTimeout = 2000;
                port.Open();

                Interlocked.CompareExchange( ref isConnected, 1, 0 );

                GetControl( port );
            }
            catch
            {
                Interlocked.CompareExchange( ref isConnected, 0, 1 );
            }
        }

        int Read( SerialPort port, byte[] buffer, int amount )
        {
            var readAmount = 0;
            var readToGo = amount - readAmount;

            try
            {
                while ( readToGo != 0 )
                {
                    readAmount += port.Read( buffer, readAmount, readToGo );
                    readToGo = amount - readAmount;
                }
            }
            catch
            {
                readAmount = -1;
            }

            return readAmount;
        }

        int Write( SerialPort port, byte[] buffer, int amount )
        {
            var writeAmount = amount;

            try
            {
                port.Write( buffer, 0, amount );
            }
            catch
            {
                writeAmount = -1;
            }

            return writeAmount;
        }

        void GetControl( SerialPort port )
        {
            var controlData = new byte[ 2 ];
            controlData[ 0 ] = (byte)((short)PacketType.GetControl & 0x00FF);
            controlData[ 1 ] = (byte)(((short)PacketType.GetControl & 0xFF00) >> 8);

            Write( port, controlData, controlData.Length );
        }

        void SetControl( SerialPort port )
        {
            var controlData = new byte[ 4 ];
            controlData[ 0 ] = (byte)((short)PacketType.SetControl & 0x00FF);
            controlData[ 1 ] = (byte)(((short)PacketType.SetControl & 0xFF00) >> 8);
            controlData[ 2 ] = (byte)Interlocked.CompareExchange( ref coupling, 0, 0 );

            Write( port, controlData, controlData.Length );
        }

        PacketType Synchronize( SerialPort port )
        {
            var packetMarker = 0;
            var readResult = 0;
            var attempts = 0;

            do
            {
                attempts++;
                readResult = Read( port, synchBuffer, 2 );

                if ( readResult == -1 )
                    return PacketType.None;

                packetMarker = (synchBuffer[ 0 ] | (synchBuffer[ 1 ] << 8));

                if ( packetMarker == 5000 || packetMarker == 5001 || packetMarker == 5002 )
                {
                    Interlocked.CompareExchange( ref isSynchronized, 1, 0 );
                    return (PacketType)packetMarker;
                }
                else
                {
                    Interlocked.CompareExchange( ref isSynchronized, 0, 1 );
                }

            } while ( attempts < 10000 );

            return PacketType.None;
        }

        protected unsafe override void Run( object state )
        {
            //
            // Kaikki tässä nähdyt koko arvot pitäisi siirtää saatavaksi laitteelta itseltään!
            //
            var payloadSamples = 256;
            var payloadBytesPerSample = 2;
            var payloadBytes = payloadSamples * payloadBytesPerSample;
            var payloadBytesBuffer = new byte[ payloadBytes ];
            var payloadSamplesBuffer = new float[ payloadSamples ];

            using ( var port = new SerialPort() )
            {
                // Yhdistetään porttiin.
                Connect( port );

                while ( true )
                {
                    // Olemmeko saaneet käskyn lopettaa?
                    if ( stopEvent.WaitOne( 0 ) )
                        return;

                    // Olemmeko saanet käskyn lähettää kontrolli paketti?
                    if ( controlEvent.WaitOne( 0 ) )
                    {
                        SetControl( port );

                        // Kysytään välittömästi asetukset jotta pysymme samassa tilassa laitteen kanssa.
                        Thread.Sleep( 10 ); // HACK varmistetaan että datayhteys ehtii toimimaan.
                        GetControl( port );
                        Thread.Sleep( 10 ); // HACK varmistetaan että datayhteys ehtii toimimaan.
                    }

                    //
                    // Synkronisoidaan.
                    //
                    switch ( Synchronize( port ) )
                    {
                        //
                        // Yhdistetään uudelleen virheen sattuessa.
                        //
                        case PacketType.None:
                            Connect( port );
                            break;
                        //
                        // Prosessoidaan configuraatiopaketti.
                        //
                        case PacketType.GetControl:
                            var controlData = new byte[ 2 ];

                            Read( port, controlData, controlData.Length );

                            Interlocked.Exchange( ref coupling, (controlData[ 0 ] & 0x01) );
                            break;
                        //
                        // Prosessoidaan datapaketti.
                        //
                        case PacketType.Payload:
                            Read( port, payloadBytesBuffer, payloadBytes );

                            fixed ( byte* bytesPtr = payloadBytesBuffer )
                            fixed ( float* samplesPtr = payloadSamplesBuffer )
                            {
                                int sampleCounter = 0;
                                int sampleRaw;
                                float sampleActual;

                                //
                                // Kaikki tässä nähdyt skaalaus yms. arvot pitäisi siirtää saatavaksi laitteelta itseltään!
                                //
                                for ( var i = 0; i < payloadBytes; i += 2 )
                                {
                                    // Yhdistetään ensin tavut 10-bittiseksi datapisteeksi.
                                    sampleRaw = bytesPtr[ i + 0 ] | ((bytesPtr[ i + 1 ] & 0x03) << 8);
                                    // Skaalataan datapiste jännitteeksi ADC:n mittausalueella.
                                    sampleActual = ScopeMath.Map( sampleRaw, 0.0f, 1023.0f, 0f, 5.0f );
                                    // Siirretään datapiste alkuperäiselle jännitetasolle.
                                    sampleActual = 1.65f - sampleActual;
                                    // Skaalataan datapiste alkuperäisen suuruiseksi jännitteeksi.
                                    sampleActual *= 11f;

                                    samplesPtr[ sampleCounter++ ] = sampleActual;
                                }
                            }

                            input.Write( payloadSamplesBuffer, payloadSamplesBuffer.Length );
                            break;
                    }
                }
            }
        }
    }
}

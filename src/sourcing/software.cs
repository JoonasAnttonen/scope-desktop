﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using System.IO.Ports;

namespace ScopeApp
{
    /// <summary>
    /// Ohjelmisto lähteen funktio.
    /// </summary>
    public enum SoftwareFunction
    {
        /// <summary> Siniaalto. </summary>
        Sine,
        /// <summary> Kanttiaalto. </summary>
        Square,
        /// <summary> Kolmioaalto. </summary>
        Triangle,
        /// <summary> Sahalaita. </summary>
        Sawtooth
    }

    /// <summary>
    /// Ohjelmisto lähde.
    /// </summary>
    public class SoftwareSource : ScopeSource
    {
        readonly Random random = new Random();

        //SoftwareFunction function;
        ScopeCoupling coupling;

        float vpp;
        float vdc;
        float frequencyHz;
        float samplerate;
        float noiseAmplitude;
        double t;

        public override ScopeCoupling Coupling
        {
            get { return coupling; }
            set { coupling = value; }
        }

        public override float Samplerate
        {
            get { return samplerate; }
        }

        public SoftwareSource( double vpp, double vdc, double frequencyHz, double samplerateHz, double noiseAmplitude = 0.0 )
        {
            this.vpp = (float)vpp;
            this.vdc = (float)vdc;
            this.frequencyHz = (float)frequencyHz;
            this.samplerate = (float)(1.0 / samplerateHz);
            this.noiseAmplitude = (float)noiseAmplitude;
        }

        protected override void Run( object state )
        {
            while ( true )
            {
                // Olemmeko saaneet käskyn lopettaa?
                if ( stopEvent.WaitOne( 0 ) )
                    return;         

                var missingSamples = input.Capacity - input.Length;

                for ( var i = 0; i < missingSamples; i++ )
                    input.Write( GenerateSample( t++ ) );

                Thread.Sleep( 100 );
            }
        }

        float GenerateSample( double time )
        {
            if ( coupling == ScopeCoupling.GND )
                return 0f;

            var noise = noiseAmplitude * (float)random.NextDouble();
            var sample = (float)Math.Sin( 2 * Math.PI * (samplerate * frequencyHz * time) );
            sample *= vpp;

            if ( coupling == ScopeCoupling.DC )
                sample += vdc + noise;
            else
                sample *= noise;

            return sample;
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace ScopeApp
{
    /// <summary> 
    /// Oskilloskoopin näyteikkunan tiedot. 
    /// </summary>
    public struct ScopeWindow
    {
        /// <summary> Tarkka vaakaruutujen määrä. </summary>
        public readonly float HorizontalDivisions;
        /// <summary> Tarkka pystyruutujen määrä. </summary>
        public readonly float VerticalDivisions;

        /// <summary> Luo uudet näyteikkunan tiedot. </summary>
        /// <param name="horizontalDivisions">Tarkka vaakaruutujen määrä.</param>
        /// <param name="verticalDivisions">Tarkka pystyruutujen määrä.</param>
        public ScopeWindow( float horizontalDivisions, float verticalDivisions )
        {
            this.HorizontalDivisions = horizontalDivisions;
            this.VerticalDivisions = verticalDivisions;
        }
    }

    /// <summary> 
    /// Oskilloskoopin triggerin tila. 
    /// </summary>
    public enum ScopeTriggerMode
    {
        Auto,
        Normal,
        Single
    }

    /// <summary>
    /// Oskilloskoopin kontrolleri.
    /// </summary>
    public class ScopeController
    {
        ScopeWindow window;
        ScopeTriggerMode triggerMode;
        ScopeTrigger trigger;

        List<ScopeChannel> channels;

        int sampleDelay = 0;
        bool stopped = false;
        //float timeDelay = 0f;
        float timeDivision = 0.0002f;

        public bool Running
        {
            get { return !stopped; }
            set
            {
                stopped = !value;

                if ( !stopped && triggerMode == ScopeTriggerMode.Single && trigger.Triggered )
                {
                    trigger.Reset();
                    triggerChannel.Source.Clear( channelBufferLength );
                }
            }
        }

        public float Samplerate
        {
            get { return triggerChannel.Samplerate; }
        }

        public int SampleDelay
        {
            get { return sampleDelay; }
            set { sampleDelay = value; }
        }

        public ScopeTrigger Trigger
        {
            get { return trigger; }
        }

        public ScopeTriggerMode TriggerMode
        {
            get { return triggerMode; }
            set
            {
                triggerMode = value;
                trigger.Reset();

                for ( var i = 0; i < channels.Count; i++ )
                {
                    var cha = channels[ i ];
                    var src = cha.Source;

                    cha.Clear();
                    src.Clear();
                }
            }
        }

        public float TimeDivision
        {
            get { return timeDivision; }
            set { timeDivision = value; }
        }

        public float TimeDelay
        {
            get { return (sampleDelay * triggerChannel.Samplerate); }
        }

        int channelBufferLength = 4096;
        float triggerHoldoff = 0.001f;
        float[] triggerBuffer;
        ScopeChannel triggerChannel;

        /// <summary> 
        /// Luo uuden kontrollerin. 
        /// </summary>
        public ScopeController()
        {
            trigger = ScopeTrigger.Empty;
            channels = new List<ScopeChannel>();
            triggerBuffer = new float[ channelBufferLength ];
        }

        /// <summary> 
        /// Asettaa kontrollerin näyte ikkunan tiedot. 
        /// </summary>
        /// <param name="window">Ikkuna joka asetetaan.</param>
        public void SetWindow( ScopeWindow window )
        {
            this.window = window;
        }

        /// <summary> 
        /// Asettaa kontrollerin triggerin. 
        /// </summary>
        /// <param name="trigger">Triggeri joka asetetaan.</param>
        public void SetTrigger( ScopeTrigger trigger )
        {
            this.trigger = trigger;
        }

        /// <summary> 
        /// Asettaan kontrollerin triggerin kanavan. 
        /// </summary>
        /// <param name="channel">Kanava joka asetetaan.</param>
        public void SetTriggerChannel( ScopeChannel channel )
        {
            this.triggerChannel = channel;
        }

        /// <summary> 
        /// Lisää kanavan kontrolleriin. 
        /// </summary>
        /// <param name="channel">Kanava joka lisätään.</param>
        public void AddChannel( ScopeChannel channel )
        {
            // Varmistetaan että kanavan puskuri on oikean kokoinen.
            if ( channel.Buffer.Capacity != channelBufferLength )
                channel.Buffer = new ScopeBuffer( channelBufferLength );

            channels.Add( channel );
        }

        /// <summary> 
        /// Poistaa kanavan kontrollerista. 
        /// </summary>
        /// <param name="channel">Kanava joka poistetaan.</param>
        public bool RemoveChannel( ScopeChannel channel )
        {
            return channels.Remove( channel );
        }

        /// <summary> 
        /// Päivittää kontrollerin. 
        /// </summary>
        public void Update()
        {
            var triggerSource = triggerChannel.Source;
            var triggerSample = 0;
            var triggerSkip = (int)(triggerHoldoff / triggerSource.Samplerate);
            var transferLength = 0;

            //
            // Varmistetaan että trigger taso on ikkunan sisällä.
            //
            var windowMin = (window.VerticalDivisions * triggerChannel.VerticalsPerDivision) / -2f;
            var windowMax = -windowMin;

            for ( var i = 0; i < trigger.Cursors; i++ )
            {
                if ( (trigger[ i ] + triggerChannel.VerticalOffset) < windowMin )
                {
                    trigger[ i ] = (float)Math.Round( (trigger[ i ] + windowMin - (trigger[ i ] + triggerChannel.VerticalOffset)), 2 );
                }
                if ( (trigger[ i ] + triggerChannel.VerticalOffset) > windowMax )
                {
                    trigger[ i ] = (float)Math.Round( (trigger[ i ] - (trigger[ i ] + triggerChannel.VerticalOffset) - windowMax), 2 );
                }
            }

            //
            // Olemmeko käynnissä?
            //
            if ( !stopped )
            {
                if ( triggerSource.AvailableSamples < triggerBuffer.Length )
                    return;

                // SINGLE mode ei tietenkää alusta triggeriä.
                if ( triggerMode != ScopeTriggerMode.Single )
                    trigger.Reset();

                // Jos trigger on jo aktivoitunut tai näytteitä ei ole tarpeeksi, poistutaan samantien.
                if ( trigger.Triggered )
                    return;

                // Kurkataan sampleja lähteestä.
                triggerSource.Peek( triggerBuffer, triggerBuffer.Length );

                //
                // Testataan trigger.
                //
                if ( trigger.Sense( triggerBuffer, triggerSkip, ref triggerSample ) )
                {
                    transferLength = triggerSample + 1 + (channelBufferLength / 2);

                    /*var edge0 = triggerSample;
                    var edge1 = 0;

                    if ( trigger.Sense( triggerBuffer, triggerSample + 1, ref edge1 ) )
                    {
                        var edgeDist = edge1 - edge0 - 2;
                        var period = edgeDist * triggerChan.Samplerate;
                        var frequency = 1.0 / period;
                    }*/
                }
                //
                // Vaikka trigger ei aktivoitunut, AUTO tila 'pakottaa' sen silti.
                //
                else if ( triggerMode == ScopeTriggerMode.Auto )
                {
                    transferLength = channelBufferLength;
                }
            }

            //
            // Päivitetään kanavat.
            // 
            for ( var i = 0; i < channels.Count; i++ )
            {
                var cha = channels[ i ];
                var src = channels[ i ].Source;
                var dst = channels[ i ].Buffer;

                //
                // Trigger ei aktivoitunut emmekä ole AUTO tilassa joten tyhjennetään kanava ja hylätään kanavan verran näytteitä.
                //
                if ( transferLength == 0 )
                {
                    // Jos olemme pysäytettynä, haluamme pitää vanhat näytteet kanavalla.
                    if ( !stopped )
                    {
                        cha.Clear();
                    }

                    src.Clear( channelBufferLength );
                }
                //
                // Trigger on aktivoitunut joten luetaan näytteitä kanavaan.
                // 
                else
                {
                    // Ei tuhlata aikaa päivittämään näkymättömiä kanavia tai odottelemaan vajaita kanavia.
                    if ( cha.Visible && (src.AvailableSamples >= transferLength) )
                    {
                        src.Transfer( transferLength, dst );
                    }
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using System.Globalization;

namespace ScopeApp
{
    /// <summary>
    /// Kokoelma näppäriä funktioita.
    /// </summary>
    public static class ScopeUtil
    {
        /// <summary> Palauttaa sekunti arvoa kuvaavan tekstin päätteineen. (0.01 -> 10 ms) </summary>
        /// <param name="value">Arvo jota tarkastellaan.</param>
        public static string FormatSeconds( float value )
        {
            // Itseisarvo jotta toimii myös negatiivisella puolella.
            var abs = Math.Abs( value );

            // Kun arvo on 0, olkoon pääte 'perus'.
            if ( abs == 0f )
                return "0 s";

            // Yksinkertaisesti katsotaan:
            // Pienimmästä alkaen millä alueella luku...
            // Kerrotaan luku kokonaiseksi kyseisellä alueella...
            // Lisätään asianmukainen pääte.
            if ( abs > 1e-9f && abs < 1e-6f )
                return (value * 1e9f).ToString( CultureInfo.InvariantCulture ) + " ns";
            else if ( abs >= 1e-6f && abs < 1e-3f )
                return (value * 1e6f).ToString( CultureInfo.InvariantCulture ) + " us";
            else if ( abs >= 1e-3f && abs < 1 )
                return (value * 1e3f).ToString( CultureInfo.InvariantCulture ) + " ms";
            else
                return value.ToString( CultureInfo.InvariantCulture ) + " s";
        }

        /// <summary> Palauttaa jännite arvoa kuvaavan tekstin päätteineen. (0.01 -> 10 mV) </summary>
        /// <param name="value">Arvo jota tarkastellaan.</param>
        public static string FormatVolts( float value )
        {
            // Itseisarvo jotta toimii myös negatiivisella puolella.
            var abs = Math.Abs( value );

            // Kun arvo on 0, olkoon pääte 'perus'.
            if ( abs == 0f )
                return "0 V";

            // Yksinkertaisesti katsotaan:
            // Pienimmästä alkaen millä alueella luku...
            // Kerrotaan luku kokonaiseksi kyseisellä alueella...
            // Lisätään asianmukainen pääte.
            if ( abs >= 1e-9f && abs < 1e-6f )
                return (value * 1e9f).ToString( CultureInfo.InvariantCulture ) + " nV";
            else if ( abs >= 1e-6f && abs < 1e-3f )
                return (value * 1e6f).ToString( CultureInfo.InvariantCulture ) + " uV";
            else if ( abs >= 1e-3f && abs < 1 )
                return (value * 1e3f).ToString( CultureInfo.InvariantCulture ) + " mV";
            else
                return value.ToString( CultureInfo.InvariantCulture ) + " V";
        }
    }

    /// <summary>
    /// Kokoelma näppäriä matematiikka funktioita.
    /// </summary>
    public static class ScopeMath
    {
        /// <summary> Laskee lähimmän parillisen tasaluvun. </summary>
        /// <param name="value">Luku jota tarkastella.</param>
        public static int Even( float value )
        {
            // Pyöristetään tasaluvuksi...
            var v = (int)Math.Round( value );

            // Tarkistetaan onko parillinen ja jollei ole, lisätään yksi.
            return (((v % 2) == 0) ? v : v + 1);
        }

        /// <summary> Laskee lähimmän parillisen tasaluvun. </summary>
        /// <param name="value">Luku jota tarkastella.</param>
        public static int Even( int value )
        {
            // Tarkistetaan onko parillinen ja jollei ole, lisätään yksi.
            return (((value % 2) == 0) ? value : value + 1);
        }

        /// <summary> Skaalaa arvon annetulta väliltä toiselle. </summary>
        /// <param name="x">Arvo jota tarkastellaan.</param>
        /// <param name="in_min">Pienin alkuperäinen arvo.</param>
        /// <param name="in_max">Suurin alkuperäinen arvo.</param>
        /// <param name="out_min">Pienin luku tulos välillä.</param>
        /// <param name="out_max">Suurin luku tulos välillä.</param>
        public static float Map( float x, float in_min, float in_max, float out_min, float out_max )
        {
            return ((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min);
        }

        /// <summary> Rajoittaa arvon annetulle alueelle. </summary>
        /// <param name="value">Arvo jota tarkastellaan.</param>
        /// <param name="min">Pienin arvo.</param>
        /// <param name="max">Suurin arvo.</param>
        public static int Clamp( int value, int min, int max )
        {
            return Math.Max( Math.Min( value, max ), min );
        }

        /// <summary> Rajoittaa arvon annetulle alueelle. </summary>
        /// <param name="value">Arvo jota tarkastellaan.</param>
        /// <param name="min">Pienin arvo.</param>
        /// <param name="max">Suurin arvo.</param>
        public static float Clamp( float value, float min, float max )
        {
            return Math.Max( Math.Min( value, max ), min );
        }

        /// <summary> Laskee seuraavan kahden potenssin. </summary>
        /// <param name="n">Arvo jota tarkastellaan.</param>
        public static int NextPowerOf2( int n )
        {
            return (int)Math.Pow( 2, Math.Floor( Math.Log( n, 2 ) ) + 1 );
        }

        /// <summary> Ilmoittaa onko arvo kahden potenssi. </summary>
        /// <param name="n">Arvo jota tarkastellaan.</param>
        public static bool IsPowerOfTwo( int x )
        {
            return (x != 0) && ((x & (x - 1)) == 0);
        }
    }
}

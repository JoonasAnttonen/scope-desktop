﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScopeApp
{
    /// <summary>
    /// Reunan tyyppi.
    /// </summary>
    public enum EdgeType
    {
        /// <summary> Nouseva. </summary>
        Rising,
        /// <summary> Laskeva. </summary>
        Falling,
        /// <summary> Nouseva tai laskeva. </summary>
        Both
    }

    /// <summary>
    /// Reuna triggeri.
    /// </summary>
    public class EdgeTrigger : ScopeTrigger
    {
        EdgeType edge;
        float level;

        public override int Cursors { get { return 1; } }
        public override float this[ int i ]
        {
            get { return this.level; }
            set { this.level = value; }
        }

        /// <summary> Reunan tyyppi. </summary>
        public EdgeType Edge
        {
            get { return edge; }
            set { edge = value; }
        }

        public EdgeTrigger( EdgeType edge, double level )
        {
            this.level = (float)level;
            this.edge = edge;
        }

        public unsafe override bool Sense( float[] samples, int begin, ref int sample )
        {
            // Otetaan useampi näyte kerrallaan käsittelyyn jotta häiriöt eivät pääse niin paljoa vaikuttamaan reunan tunnistukseen.
            float sample0, sample1, sample2, sample3, sample4, sample5;

            fixed ( float* buf = samples )
            {
                for ( var i = begin; ((i + 5) < samples.Length); i++ )
                {
                    sample0 = buf[ i + 0 ];
                    sample1 = buf[ i + 1 ];
                    sample2 = buf[ i + 2 ];
                    sample3 = buf[ i + 3 ];
                    sample4 = buf[ i + 4 ];
                    sample5 = buf[ i + 5 ];

                    //
                    // Nouseva reuna?
                    //
                    if ( sample0 < level &&
                         sample1 <= level && sample2 <= level &&
                         sample3 >= level && sample4 >= level &&
                         sample5 > level )
                    {
                        if ( edge == EdgeType.Rising || edge == EdgeType.Both )
                        {
                            triggered = true;
                            triggeredOnSample = i;
                            sample = i;
                            return true;
                        }
                    }

                    //
                    // Laskeva reuna?
                    //
                    if ( sample0 > level &&
                         sample1 >= level && sample2 >= level &&
                         sample3 <= level && sample4 <= level &&
                         sample5 < level )
                    {
                        if ( edge == EdgeType.Falling || edge == EdgeType.Both )
                        {
                            triggered = true;
                            triggeredOnSample = i;
                            sample = i;
                            return true;
                        }
                    }
                }
            }

            return false;
        }
    }
}

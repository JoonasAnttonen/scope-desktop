﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScopeApp
{
    /// <summary>
    /// Abstraktio triggereille.
    /// </summary>
    public abstract class ScopeTrigger
    {
        class NullTrigger : ScopeTrigger
        {
            public override int Cursors { get { return 0; } }
            public override float this[ int i ] { get { throw new InvalidOperationException(); } set { throw new InvalidOperationException(); } }
            public override bool Sense( float[] samples, int begin, ref int sample ) { return false; }
        }

        /// <summary> Tyhjä triggeri. </summary>
        public static readonly ScopeTrigger Empty = new NullTrigger();

        protected bool triggered;
        protected int triggeredOnSample;

        public int TriggeredOnSample
        {
            get { return triggeredOnSample; }
        }

        /// <summary> Onko trigger aktivoitunut? </summary>
        public bool Triggered
        {
            get { return triggered; }
        }

        /// <summary> Alusta trigger. </summary>
        public void Reset()
        {
            triggered = false;
        }

        public abstract int Cursors { get; }
        public abstract float this[ int i ] { get; set; }
        public abstract bool Sense( float[] samples, int begin, ref int sample );
    }
}

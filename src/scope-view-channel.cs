﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kodo.Graphics;
using Kodo.Graphics.Style;

namespace ScopeApp
{
    public class ScopeChannelView : StyleControl
    {
        float channelAreaWidth = 200f;

        const string DC = "DC";
        const string AC = "AC";
        const string GND = "GND";
        const string SYNCH = "SYNCH";
        const string CONN = "CONN";
        const string NONE = "NONE";

        StyleText hugeText;

        Bitmap cursorLeftBitmap;
        Bitmap cursorRightBitmap;
        Bitmap cursorDownBitmap;

        int channelThatIsHovered = -1;

        class ChannelData
        {
            public ScopeChannel Channel { get; set; }
            public Rectangle Area { get; set; }
        }

        List<ChannelData> channels = new List<ChannelData>();

        public void AddChannel( ScopeChannel c )
        {
            var channel = new ChannelData();
            channel.Channel = c;

            channels.Add( channel );

            Update();
        }

        public ScopeChannelView( StyleWindow window )
            : base( window )
        {
        }

        protected override void OnMouseWheel( Mouse mouse )
        {
            base.OnMouseWheel( mouse );

            if ( channelThatIsHovered != -1 )
            {
                channels[ channelThatIsHovered ].Channel.ChangeVerticalDivision( (mouse.WheelDelta > 0) ? -1 : 1 );
            }
        }

        protected override void OnMouseUp( Mouse mouse )
        {
            base.OnMouseUp( mouse );
        }

        protected override void OnMouseDown( Mouse mouse )
        {
            base.OnMouseDown( mouse );

            if ( channelThatIsHovered != -1 )
            {
                if ( mouse.Button == MouseButton.Left )
                {
                    channels[ channelThatIsHovered ].Channel.Visible = !channels[ channelThatIsHovered ].Channel.Visible;
                }
                if ( mouse.Button == MouseButton.Right )
                {
                    channels[ channelThatIsHovered ].Channel.Source.Coupling = (channels[ channelThatIsHovered ].Channel.Source.Coupling == ScopeCoupling.DC ? ScopeCoupling.AC : ScopeCoupling.DC);
                }
            }
        }

        protected override void OnMouseLeave( Mouse mouse )
        {
            base.OnMouseLeave( mouse );

            channelThatIsHovered = -1;
        }

        protected override void OnMouseMove( Mouse mouse )
        {
            base.OnMouseDown( mouse );

            channelThatIsHovered = -1;

            for ( var i = 0; i < channels.Count; i++ )
            {
                var channel = channels[ i ];

                if ( channel.Area.Contains( mouse.Position ) )
                {
                    channelThatIsHovered = i;
                    Update();
                    return;
                }
            }
        }

        protected override void OnLoad( Context context )
        {
            hugeText = new StyleText( "Montserrat", 16f );

            cursorLeftBitmap = new Bitmap( context, ScopeGlobal.Icons[ ScopeIcon.PinLeft ].Source );
            cursorRightBitmap = new Bitmap( context, ScopeGlobal.Icons[ ScopeIcon.PinRight ].Source );
            cursorDownBitmap = new Bitmap( context, ScopeGlobal.Icons[ ScopeIcon.PinDown ].Source );
        }

        protected override void OnUpdate( Context context )
        {
            var clip = new Rectangle( this.Area.Dimensions );

            var totalWidth = channelAreaWidth * channels.Count;
            var channelArea = Rectangle.FromXYWH( clip.Width / 2f - totalWidth, clip.Top, channelAreaWidth, clip.Height );

            for ( var i = 0; i < channels.Count; i++ )
            {
                channelArea = channelArea.Translate( channelAreaWidth, 0f );
                channels[ i ].Area = channelArea;
            }
        }

        public new void Update()
        {
            Refresh();
        }

        protected override void OnDraw( Context context )
        {
            var clip = new Rectangle( this.Area.Dimensions );

            context.AntialiasMode = AntialiasMode.PerPrimitive;
            SharedBrush.Opacity = 1f;

            SharedStyle.Align( clip );
            context.FillRectangle( clip, SharedStyle.Background );

            for ( var i = 0; i < channels.Count; i++ )
            {
                var channel = channels[ i ];
                var channelClip = new Rectangle( channel.Area.Left + 2f, channel.Area.Top + 2f, channel.Area.Right - 2f, channel.Area.Bottom - 2f );

                SharedBrush.Color = (channel.Channel.Visible ? channel.Channel.Color : Color.Black);

                hugeText.HorizontalAlignment = TextAlignment.Center;
                hugeText.VerticalAlignment = TextAlignment.Center;
                context.DrawText( ScopeUtil.FormatVolts( channel.Channel.VerticalsPerDivision ), hugeText, channelClip, SharedBrush );

                var cursorIconRect = new Rectangle( ScopeGlobal.IconMedium ).CenterTo( channelClip );
                cursorIconRect.Translate( -(cursorIconRect.Left - channelClip.Left) + 2f, 0f );

                context.AntialiasMode = AntialiasMode.Aliased;
                context.FillOpacityMask( cursorRightBitmap, SharedBrush, cursorIconRect );
                context.AntialiasMode = AntialiasMode.PerPrimitive;

                SharedText.Align( TextAlignment.Trailing, TextAlignment.Leading );

                switch ( channel.Channel.Source.Coupling )
                {
                    case ScopeCoupling.DC:
                        context.DrawText( DC, SharedText.Small, channelClip, SharedBrush );
                        break;
                    case ScopeCoupling.AC:
                        context.DrawText( AC, SharedText.Small, channelClip, SharedBrush );
                        break;
                    case ScopeCoupling.GND:
                        context.DrawText( GND, SharedText.Small, channelClip, SharedBrush );
                        break;
                }

                SharedText.Align( TextAlignment.Trailing, TextAlignment.Trailing );

                if ( channel.Channel.Visible )
                {
                    if ( channel.Channel.Source.Synchronized )
                    {
                        SharedBrush.Color = Color.LightGreen;
                        context.DrawText( SYNCH, SharedText.Small, channelClip, SharedBrush );
                    }
                    else if ( channel.Channel.Source.Connected )
                    {
                        SharedBrush.Color = Color.OrangeRed;
                        context.DrawText( CONN, SharedText.Small, channelClip, SharedBrush );
                    }
                    else
                    {
                        SharedBrush.Color = Color.IndianRed;
                        context.DrawText( NONE, SharedText.Small, channelClip, SharedBrush );
                    }
                }
                else
                {
                    SharedBrush.Color = Color.Black;
                    context.DrawText( NONE, SharedText.Small, channelClip, SharedBrush );
                }

                SharedStyle.Align( channel.Area );
                context.FillRectangle( channel.Area, SharedStyle.Foreground );

                if ( channelThatIsHovered == i )
                {
                    context.FillRectangle( channel.Area, SharedStyle.ForegroundHover );
                }

                SharedBrush.Color = Color.Black;
                context.DrawRectangle( channel.Area, SharedBrush, 2f );
            }

            SharedBrush.Color = Color.Black;
            context.DrawRectangle( clip, SharedBrush, 2f );
        }
    }
}

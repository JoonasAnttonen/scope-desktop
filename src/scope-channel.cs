﻿using System;

namespace ScopeApp
{
    /// <summary>
    /// Oskilloskoopin kanava.
    /// </summary>
    public class ScopeChannel
    {
        /// <summary> 
        /// Vertikaalisen skaalan asetus mahdollisuudet. 
        /// </summary>
        public static readonly float[] VoltDivisions = new float[] { 10.0f, 5.0f, 2.0f, 1.0f, 0.5f, 0.2f, 0.1f, 0.05f, 0.02f, 0.01f, 0.005f, 0.002f, 0.001f };

        /// <summary> 
        /// Lähteetön kanava. 
        /// </summary>
        public static readonly ScopeChannel Empty = new ScopeChannel( "CH" );

        float voltsPerDivision = 1.0f;
        ScopeSource source = ScopeSource.Empty;
        ScopeBuffer buffer = new ScopeBuffer( 2 );

        /// <summary> 
        /// Luo uuden lähteettömän kanavan. 
        /// </summary>
        /// <param name="name">Kanavan nimi.</param>
        public ScopeChannel( string name ) : this( name, ScopeSource.Empty, false ) { }
        /// <summary>
        /// Luo uuden kanavan.
        /// </summary>
        /// <param name="name">Kanavan nimi.</param>
        /// <param name="source">Kanavan lähde.</param>
        /// <param name="visible">Kanavan näkyvyys.</param>
        public ScopeChannel( string name, ScopeSource source, bool visible = true )
        {
            this.Name = name;
            this.Source = source;
            this.Visible = visible;
        }

        /// <summary>
        /// Muuttaa kanavan vertikaalista skaalaa suuntaan tai toiseen.
        /// </summary>
        /// <param name="amount">Suunta ja määrä.</param>
        public void ChangeVerticalDivision( int amount )
        {
            var voltDivision = voltsPerDivision;
            var voltDivisionsPos = 0;

            for ( var i = 0; i < VoltDivisions.Length; i++ )
            {
                if ( VoltDivisions[ i ] == voltDivision )
                    voltDivisionsPos = i;
            }

            voltDivisionsPos -= amount;

            if ( voltDivisionsPos < 0 )
                voltDivisionsPos = 0;
            else if ( voltDivisionsPos > VoltDivisions.Length - 1 )
                voltDivisionsPos = VoltDivisions.Length - 1;

            voltsPerDivision = VoltDivisions[ voltDivisionsPos ];
        }

        /// <summary> 
        /// Kanavan nimi. 
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary> 
        /// Kanavan näkyvyys. 
        /// </summary>
        public bool Visible
        {
            get;
            set;
        }

        /// <summary> 
        /// Kanavan väri. 
        /// </summary>
        public Kodo.Graphics.Color Color
        {
            get;
            set;
        }

        /// <summary> 
        /// Kanavan pystysuuntainen skaala.
        /// </summary>
        public float VerticalsPerDivision
        {
            get { return voltsPerDivision; }
            set { voltsPerDivision = value; }
        }

        /// <summary> 
        /// Kanavan pystysuuntainen offset.
        /// </summary>
        public float VerticalOffset
        {
            get;
            set;
        }

        /// <summary> 
        /// Kanavan lähde. 
        /// </summary>
        public ScopeSource Source
        {
            get { return source; }
            set { source = value; }
        }

        /// <summary> 
        /// Kanavan puskuri. 
        /// </summary>
        public ScopeBuffer Buffer
        {
            get { return buffer; }
            set { buffer = value; }
        }

        /// <summary> 
        /// Tyhjentää näytepuskurin. 
        /// </summary>
        public void Clear()
        {
            buffer.Clear();
        }

        /// <summary> 
        /// Lukee näytteen näytepuskurista halutusta kohdasta.
        /// </summary>
        /// <param name="sample">Kohta.</param>
        public float Read( int sample )
        {
            return buffer.Read( sample );
        }

        /// <summary> 
        /// Kanavan näytepuskurin pituus. 
        /// </summary>
        public int Samples
        {
            get { return buffer.Capacity; }
        }

        /// <summary>
        /// Kanavan näytteenotto periodi. 
        /// </summary>
        public float Samplerate
        {
            get { return source.Samplerate; }
        }
    }
}

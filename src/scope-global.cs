﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using Kodo.Graphics;
using Kodo.Graphics.Style;

[assembly: AssemblyTitle( ScopeApp.ScopeGlobal.ApplicationName )]
[assembly: AssemblyProduct( ScopeApp.ScopeGlobal.ApplicationName )]
[assembly: AssemblyCopyright( "Copyright © Joonas Anttonen 2015" )]
[assembly: ComVisible( false )]
[assembly: Guid( "9ead5f95-54cc-4055-a8e2-cd30ee0386ab" )]
[assembly: AssemblyVersion( "1.0.0.0" )]
[assembly: AssemblyFileVersion( "1.0.0.0" )]

namespace ScopeApp
{
    public enum ScopeIcon
    {
        PinDown,
        PinLeft,
        PinRight,
    }

    public class ScopeGlobal
    {
        public const string ApplicationName = "scope-desktop";

        public static readonly Size IconBig = new Size( 42, 42 );
        public static readonly Size IconMedium = new Size( 32, 32 );
        public static readonly Size IconSmall = new Size( 20, 20 );
        public static Dictionary<ScopeIcon, StyleImage> Icons;

        static void Main( string[] args )
        {
            using ( var windowManager = new WindowManager() )
            {
                //
                // BUG BUG BUG 
                //
                // Normaalisti tämä olisi suoraan alkuperäisen määrittelyn kohdalla ja olisi readonly mutta 32-bittinen buildi crashaa jostain syystä.
                // Ehdottomasti kodo-graphics-2/-style ongelma.
                // Staattiset resurssit luodaan ehkä väärässä järjestyksessä?
                //
                Icons = new Dictionary<ScopeIcon, StyleImage>()
                {
                    { ScopeIcon.PinDown, new StyleImage( ScopeApp.Properties.Resources.pin92, IconMedium ) },
                    { ScopeIcon.PinLeft, new StyleImage( ScopeApp.Properties.Resources.pin_left, IconMedium ) },
                    { ScopeIcon.PinRight, new StyleImage( ScopeApp.Properties.Resources.pin_right, IconMedium ) },
                };

                var mainWindowSettings = new WindowSettings();
                mainWindowSettings.Area = Rectangle.FromXYWH( 100, 100, 1009, 686 );
                mainWindowSettings.Margings = new WindowMargings( 3, 30, 3, 3 );
                mainWindowSettings.Name = ApplicationName;
                var mainWindow = new ScopeMainWindow( windowManager, mainWindowSettings );
                windowManager.OnUnhandledException += OnUnhandledException;
                windowManager.Run( mainWindow );
            }
        }

        static void OnUnhandledException( Exception exception )
        {
            throw exception;
        }
    }
}

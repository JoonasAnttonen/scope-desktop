﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kodo.Graphics;
using Kodo.Graphics.Style;

namespace ScopeApp
{
    public class ScopeControlView : StyleControl
    {
        ScopeController controller = new ScopeController();

        const string TRIGD = "TRIG'D";
        const string AUTO = "AUTO";
        const string WAIT = "WAIT";
        const string STOP = "STOP";
        const string NORMAL = "NORMAL";
        const string SINGLE = "SINGLE";
        const string RISE = "RISE";
        const string FALL = "FALL";
        const string BOTH = "BOTH";

        Rectangle scopeTimeDivisionArea;
        Rectangle scopeStatusArea;
        Rectangle scopeTimeDelayArea;
        Rectangle triggerModeArea;
        Rectangle triggerLevelArea;
        Rectangle triggerEdgeArea;

        bool triggerModeHovered = false;
        bool triggerModePressed = false;
        bool scopeStatusHovered = false;
        bool scopeStatusPressed = false;
        bool triggerLevelHovered = false;
        bool triggerLevelPressed = false;
        bool triggerEdgeHovered = false;
        bool triggerEdgePressed = false;

        string scopeTimeDivisionStr;
        string scopeTimeDelayStr;

        /// <summary> Näkymän kontrolleri. </summary>
        public ScopeController Controller
        {
            get { return controller; }
            set
            {
                if ( this.controller != value )
                {
                    this.controller = value;
                    Update();
                }
            }
        }

        public ScopeControlView( StyleWindow window )
            : base( window )
        {
        }

        protected override void OnMouseLeave( Mouse mouse )
        {
            base.OnMouseLeave( mouse );
            scopeStatusHovered = false;
            triggerModeHovered = false;
            triggerLevelHovered = false;
            triggerEdgeHovered = false;
        }

        protected override void OnMouseUp( Mouse mouse )
        {
            base.OnMouseUp( mouse );
            scopeStatusPressed = false;
            triggerModePressed = false;
            triggerLevelPressed = false;
            triggerEdgePressed = false;
        }

        protected override void OnMouseDown( Mouse mouse )
        {
            base.OnMouseDown( mouse );

            var leftPressed = mouse.Button == MouseButton.Left;

            if ( scopeStatusHovered )
                scopeStatusPressed = leftPressed;
            else if ( triggerModeHovered )
                triggerModePressed = leftPressed;
            else if ( triggerEdgeHovered )
                triggerEdgePressed = leftPressed;
            else if ( triggerLevelHovered )
                triggerLevelPressed = leftPressed;

            if ( scopeStatusPressed )
            {
                if ( controller.TriggerMode == ScopeTriggerMode.Single )
                    controller.Running = true;
                else
                    controller.Running = !controller.Running;
            }
            else if ( triggerModePressed )
            {
                switch ( controller.TriggerMode )
                {
                    case ScopeTriggerMode.Auto:
                        controller.TriggerMode = ScopeTriggerMode.Normal;
                        break;
                    case ScopeTriggerMode.Normal:
                        controller.TriggerMode = ScopeTriggerMode.Single;
                        break;
                    case ScopeTriggerMode.Single:
                        controller.TriggerMode = ScopeTriggerMode.Auto;
                        break;
                }
            }
            else if ( triggerEdgePressed )
            {
                if ( controller.Trigger is EdgeTrigger )
                {
                    var trig = controller.Trigger as EdgeTrigger;

                    switch ( trig.Edge )
                    {
                        case EdgeType.Rising:
                            trig.Edge = EdgeType.Falling;
                            break;
                        case EdgeType.Falling:
                            trig.Edge = EdgeType.Both;
                            break;
                        case EdgeType.Both:
                            trig.Edge = EdgeType.Rising;
                            break;
                    }
                }
            }
            else if ( triggerLevelPressed )
            {
                controller.Trigger[ 0 ] = 0f;
            }
        }

        protected override void OnMouseMove( Mouse mouse )
        {
            base.OnMouseMove( mouse );

            scopeStatusHovered = false;
            triggerModeHovered = false;
            triggerEdgeHovered = false;
            triggerLevelHovered = false;

            if ( scopeStatusArea.Contains( mouse.Position ) )
                scopeStatusHovered = true;
            else if ( triggerModeArea.Contains( mouse.Position ) )
                triggerModeHovered = true;
            else if ( triggerEdgeArea.Contains( mouse.Position ) )
                triggerEdgeHovered = true;
            else if ( triggerLevelArea.Contains( mouse.Position ) )
                triggerLevelHovered = true;
        }

        public new void Update()
        {
            scopeTimeDelayStr = ScopeUtil.FormatSeconds( 0f );
            scopeTimeDivisionStr = ScopeUtil.FormatSeconds( controller.TimeDivision );

            Refresh();
        }

        protected override void OnLoad( Context context )
        {
        }

        protected override void OnUpdate( Context context )
        {
            var clip = new Rectangle( this.Area.Dimensions );

            var boxRect = Rectangle.FromXYWH( clip.Left + clip.Width / 2 - 30f, clip.Top, 60f, clip.Height );
            scopeStatusArea = boxRect;
            scopeTimeDivisionArea = boxRect.Translate( -boxRect.Width, 0f );
            scopeTimeDelayArea = boxRect.Translate( 2 * boxRect.Width, 0f );
            boxRect = Rectangle.FromXYWH( clip.Right - 3 * 60f, clip.Top, 60f, clip.Height );
            triggerModeArea = boxRect;
            triggerLevelArea = boxRect.Translate( 60f, 0f );
            triggerEdgeArea = boxRect.Translate( 60f, 0f );
        }

        protected override void OnDraw( Context context )
        {
            var clip = new Rectangle( this.Area.Dimensions );

            context.AntialiasMode = AntialiasMode.PerPrimitive;
            SharedBrush.Opacity = 1f;

            SharedStyle.Align( clip );
            context.FillRectangle( clip, SharedStyle.Background );

            //
            // Status
            //
            SharedText.Align( TextAlignment.Center, TextAlignment.Center );
            if ( !controller.Running )
            {
                SharedBrush.Color = Color.IndianRed;
                context.DrawText( STOP, SharedText.Big, scopeStatusArea, SharedBrush );
            }
            else if ( controller.Trigger.Triggered )
            {
                if ( controller.TriggerMode == ScopeTriggerMode.Single )
                {
                    SharedBrush.Color = Color.IndianRed;
                    context.DrawText( STOP, SharedText.Big, scopeStatusArea, SharedBrush );
                }
                else
                {
                    SharedBrush.Color = Color.LightGreen;
                    context.DrawText( TRIGD, SharedText.Big, scopeStatusArea, SharedBrush );
                }
            }
            else
            {
                if ( controller.TriggerMode == ScopeTriggerMode.Auto )
                {
                    SharedBrush.Color = Color.GhostWhite;
                    context.DrawText( AUTO, SharedText.Big, scopeStatusArea, SharedBrush );
                }
                else
                {
                    SharedBrush.Color = Color.OrangeRed;
                    context.DrawText( WAIT, SharedText.Big, scopeStatusArea, SharedBrush );
                }
            }

            //
            // Time Division
            //
            SharedBrush.Color = Color.GhostWhite;
            context.DrawText( scopeTimeDivisionStr, SharedText.Big, scopeTimeDivisionArea, SharedBrush );

            //
            // Time Delay
            //
            SharedBrush.Color = Color.GhostWhite;
            context.DrawText( scopeTimeDelayStr, SharedText.Big, scopeTimeDelayArea, SharedBrush );

            //
            // Trigger Mode
            //
            SharedBrush.Color = Color.LightGreen;
            switch ( controller.TriggerMode )
            {
                case ScopeTriggerMode.Auto: context.DrawText( AUTO, SharedText.Big, triggerModeArea, SharedBrush ); break;
                case ScopeTriggerMode.Normal: context.DrawText( NORMAL, SharedText.Big, triggerModeArea, SharedBrush ); break;
                case ScopeTriggerMode.Single: context.DrawText( SINGLE, SharedText.Big, triggerModeArea, SharedBrush ); break;
            }

            //
            // Trigger Level
            //
            SharedBrush.Color = Color.GhostWhite;
            context.DrawText( ScopeUtil.FormatVolts( controller.Trigger[ 0 ] ), SharedText.Big, triggerLevelArea, SharedBrush );

            //
            // Trigger Edge
            //
            SharedBrush.Color = Color.GhostWhite;
            if ( controller.Trigger is EdgeTrigger )
            {
                switch ( (controller.Trigger as EdgeTrigger).Edge )
                {
                    case EdgeType.Rising:
                        context.DrawText( RISE, SharedText.Big, triggerEdgeArea, SharedBrush );
                        break;
                    case EdgeType.Falling:
                        context.DrawText( FALL, SharedText.Big, triggerEdgeArea, SharedBrush );
                        break;
                    case EdgeType.Both:
                        context.DrawText( BOTH, SharedText.Big, triggerEdgeArea, SharedBrush );
                        break;
                }
            }

            SharedStyle.Align( scopeStatusArea );
            if ( scopeStatusPressed )
                context.FillRectangle( scopeStatusArea, SharedStyle.ForegroundPress );
            else if ( scopeStatusHovered )
                context.FillRectangle( scopeStatusArea, SharedStyle.ForegroundHover );

            SharedStyle.Align( triggerEdgeArea );
            if ( triggerEdgePressed )
                context.FillRectangle( triggerEdgeArea, SharedStyle.ForegroundPress );
            else if ( triggerEdgeHovered )
                context.FillRectangle( triggerEdgeArea, SharedStyle.ForegroundHover );

            SharedStyle.Align( triggerModeArea );
            if ( triggerModePressed )
                context.FillRectangle( triggerModeArea, SharedStyle.ForegroundPress );
            else if ( triggerModeHovered )
                context.FillRectangle( triggerModeArea, SharedStyle.ForegroundHover );

            SharedStyle.Align( triggerLevelArea );
            if ( triggerLevelPressed )
                context.FillRectangle( triggerLevelArea, SharedStyle.ForegroundPress );
            else if ( triggerLevelHovered )
                context.FillRectangle( triggerLevelArea, SharedStyle.ForegroundHover );

            SharedBrush.Color = Color.Black;
            context.DrawRectangle( scopeTimeDivisionArea, SharedBrush, 2f );
            context.DrawRectangle( scopeTimeDelayArea, SharedBrush, 2f );
            context.DrawRectangle( triggerModeArea, SharedBrush, 2f );
            context.DrawRectangle( scopeStatusArea, SharedBrush, 2f );
            context.DrawRectangle( triggerEdgeArea, SharedBrush, 2f );
            context.DrawRectangle( triggerLevelArea, SharedBrush, 2f );
            context.DrawRectangle( clip, SharedBrush, 2f );
        }
    }
}

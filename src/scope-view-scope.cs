﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kodo.Graphics;
using Kodo.Graphics.Style;

namespace ScopeApp
{
    public class ScopeView : StyleControl
    {
        static Size gridSize = new Size( 50f, 50f );

        static readonly float[] TimeDivisions = new float[] { 0.05f, 0.02f, 0.01f, 0.005f, 0.002f, 0.001f, 0.0005f, 0.0002f, 0.0001f, 0.00005f, 0.00002f, 0.00001f, 0.000005f, 0.000002f, 0.000001f };

        Bitmap cursorLeftBitmap;
        Bitmap cursorRightBitmap;

        Bitmap gridBitmap;
        BitmapBrush gridBrush;

        StrokeStyle cursorStroke;

        int windowXOffset;
        int samplesOnGrid;
        float xMultiplier;

        float secondsPerDivision = 0.0002f;

        //int voltDivisionsPos = 3;
        int timeDivisionsPos = 4;

        bool triggerCursorMoving = false;
        int triggerCursorThatIsMoving = -1;

        bool channelCursorMoving = false;
        int channelCursorThatIsMoving = -1;

        int channelOnTop = 0;
        int channelOnTrigger = 0;

        Point mouseDownPosition;

        Rectangle clip;
        Rectangle gridClip;
        //Rectangle naviClip;
        //Rectangle naviSeekClip;

        //List<Rectangle> channelCursors = new List<Rectangle>();
        List<Rectangle> triggerCursors = new List<Rectangle>();
        ScopeTrigger trigger = ScopeTrigger.Empty;

        public Size GridSize
        {
            get { return gridSize; }
            set
            {
                if ( gridSize != value )
                {
                    gridSize = value;
                    Update();
                }
            }
        }

        public int SampleOffset
        {
            get { return windowXOffset; }
        }

        public float SecondsPerDivision
        {
            get { return secondsPerDivision; }
        }

        class ChannelData
        {
            public ScopeChannel Channel { get; set; }
            public Point[] Points { get; set; }
            public Rectangle Cursor { get; set; }
            public float YMultiplier { get; set; }
        }

        List<ChannelData> channels = new List<ChannelData>();

        public ScopeTrigger Trigger
        {
            get { return trigger; }
            set
            {
                if ( trigger != value )
                {
                    trigger = value;

                    if ( triggerCursors.Count != trigger.Cursors )
                    {
                        triggerCursors.Clear();

                        for ( var i = 0; i < trigger.Cursors; i++ )
                            triggerCursors.Add( new Rectangle() );
                    }
                }
            }
        }

        public void AddChannel( ScopeChannel c )
        {
            var channel = new ChannelData();
            channel.Channel = c;
            channel.Points = new Point[ c.Samples ];

            channels.Add( channel );

            Update();
        }

        public void SetTrigger( ScopeTrigger value )
        {
            if ( trigger != value )
            {
                trigger = value;

                if ( triggerCursors.Count != trigger.Cursors )
                {
                    triggerCursors.Clear();

                    for ( var i = 0; i < trigger.Cursors; i++ )
                        triggerCursors.Add( new Rectangle() );
                }
            }
        }

        public void SetTriggerChannel( ScopeChannel channel )
        {
            channelOnTrigger = channels.FindIndex( ( c ) => c.Channel == channel );
        }

        ChannelData ActiveChannel
        {
            get { return channels[ channelOnTop ]; }
        }

        ChannelData TriggerChannel
        {
            get { return channels[ channelOnTrigger ]; }
        }

        public ScopeView( StyleWindow window )
            : base( window )
        {
        }

        /// <summary> Update the view. </summary>
        public new void Update()
        {
            UpdatePoints();
            Refresh();
        }

        /// <summary> Change the time division by the specified amount of 'ticks'. </summary>
        /// <param name="amount">The amount of 'ticks' to change. Typically just +-1.</param>
        /// <returns>Whether the time division actually changed.</returns>
        public bool ChangeTimeDiv( int amount )
        {
            timeDivisionsPos -= amount;

            if ( timeDivisionsPos < 0 )
                timeDivisionsPos = 0;
            else if ( timeDivisionsPos > TimeDivisions.Length - 1 )
                timeDivisionsPos = TimeDivisions.Length - 1;

            if ( UpdateTimeDiv() )
            {
                Update();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary> Center the sample window to the sample buffer. </summary>
        public void CenterSampleWindow()
        {
            windowXOffset = ScopeMath.Clamp( ((channels[ 0 ].Channel.Samples / 2) - (samplesOnGrid / 2)), 0, channels[ 0 ].Channel.Samples - samplesOnGrid );
            Update();
        }

        /// <summary> Move the view vertically by the specified percent. </summary>
        /// <param name="percent">The percent to move.</param>
        /// <returns>Whether the view actually moved.</returns>
        public bool MoveY( float percent )
        {
            var verticalDivs = gridClip.Height / gridSize.Height;
            var voltsOnGrid = ActiveChannel.Channel.VerticalsPerDivision * verticalDivs;
            var offset = (float)Math.Round( ActiveChannel.Channel.VerticalOffset + voltsOnGrid * percent, 2 );

            if ( offset >= -(voltsOnGrid / 2f) && offset <= (voltsOnGrid / 2f) )
            {
                ActiveChannel.Channel.VerticalOffset = offset;
                Update();
                return true;
            }
            else
            {
                return false;
            }
        }

        bool UpdateTimeDiv()
        {
            var timeDivs = gridClip.Width / gridSize.Width;

            // Calculate test time values.
            var testTimeDiv = TimeDivisions[ timeDivisionsPos ];
            var testTimeOnGrid = testTimeDiv * timeDivs;
            var testSamplesOnGrid = ScopeMath.Even( testTimeOnGrid / channels[ 0 ].Channel.Samplerate );

            // Do we have to calculate anything?
            if ( secondsPerDivision == testTimeDiv )
                return false;

            // Is the new time division too large?
            if ( testSamplesOnGrid > ActiveChannel.Points.Length )
            {
                // Calculate exact required time division.
                testTimeDiv = (ActiveChannel.Points.Length * channels[ 0 ].Channel.Samplerate) / timeDivs;

                // Find the largest possible time division.
                for ( var i = 0; i < TimeDivisions.Length; i++ )
                {
                    // We have found the answer if the test time division is larger than the loop division.
                    if ( (TimeDivisions[ i ] / testTimeDiv) < 1.0 )
                    {
                        timeDivisionsPos = i;
                        testTimeDiv = TimeDivisions[ timeDivisionsPos ];
                        break;
                    }
                }
            }

            // Calculate original time values.
            var oldTimeDiv = secondsPerDivision;
            var oldTimeOnGrid = oldTimeDiv * timeDivs;
            var oldSamplesOnGrid = ScopeMath.Even( oldTimeOnGrid / channels[ 0 ].Channel.Samplerate );
            var oldCenterSample = (windowXOffset + (oldSamplesOnGrid / 2));

            // Calculate new time values.
            var newTimeDiv = testTimeDiv;
            var newTimeOnGrid = newTimeDiv * timeDivs;
            var newSamplesOnGrid = ScopeMath.Even( newTimeOnGrid / channels[ 0 ].Channel.Samplerate );

            // Set the new channel time division.
            secondsPerDivision = newTimeDiv;
            // Keep the window centered on the same sample.
            windowXOffset = ScopeMath.Clamp( (oldCenterSample - (newSamplesOnGrid / 2)), 0, (channels[ 0 ].Channel.Samples - newSamplesOnGrid) );
            return true;
        }

        unsafe void UpdatePoints()
        {
            var verticalDivs = gridClip.Height / gridSize.Height;
            var horizontalDivs = gridClip.Width / gridSize.Width;

            var timeOnGrid = secondsPerDivision * horizontalDivs;

            samplesOnGrid = ScopeMath.Clamp( ScopeMath.Even( timeOnGrid / channels[ 0 ].Channel.Samplerate ), 0, channels[ 0 ].Channel.Samples );

            xMultiplier = (float)(gridClip.Width / samplesOnGrid);

            windowXOffset = ScopeMath.Clamp( ((channels[ 0 ].Channel.Samples / 2) - (samplesOnGrid / 2)), 0, channels[ 0 ].Channel.Samples - samplesOnGrid );

            for ( var i = 0; i < channels.Count; i++ )
            {
                var voltsOnGrid = (channels[ i ].Channel.VerticalsPerDivision * verticalDivs);
                channels[ i ].YMultiplier = (float)(gridClip.Height / voltsOnGrid);

                float datX = gridClip.Left;
                float datY = (gridClip.Top + gridClip.Height / 2);

                int dat1i, dat2i;
                float dat0, dat1;
                float y0, y1;

                var channel = channels[ i ];

                if ( channel.Channel.Visible )
                {
                    var cursorY = ScopeMath.Clamp( gridClip.Top + gridClip.Height / 2 - channel.Channel.VerticalOffset * channel.YMultiplier, clip.Top, clip.Bottom );
                    channel.Cursor = new Rectangle( clip.Left, cursorY - ScopeGlobal.IconMedium.Height / 2, gridClip.Left, cursorY + ScopeGlobal.IconMedium.Height / 2 );

                    fixed ( Point* chPointsPtr = channel.Points )
                    {
                        for ( var s = 0; s < samplesOnGrid; s++ )
                        {
                            dat1i = s + 0;
                            dat2i = s + 1;
                            dat0 = channel.Channel.Read( windowXOffset + dat1i );
                            dat1 = channel.Channel.Read( windowXOffset + dat2i );
                            dat0 += channel.Channel.VerticalOffset;
                            dat1 += channel.Channel.VerticalOffset;
                            y0 = ScopeMath.Clamp( datY - (dat0 * channel.YMultiplier), clip.Top + 2f, clip.Bottom - 2f );
                            y1 = ScopeMath.Clamp( datY - (dat1 * channel.YMultiplier), clip.Top + 2f, clip.Bottom - 2f );

                            chPointsPtr[ dat1i ] = new Point( datX + (dat1i * xMultiplier), y0 );
                            chPointsPtr[ dat2i ] = new Point( datX + (dat2i * xMultiplier), y1 );
                        }
                    }
                }
            }

            for ( var i = 0; i < triggerCursors.Count; i++ )
            {
                var cursorY = gridClip.Top + gridClip.Height / 2 - (trigger[ i ] + TriggerChannel.Channel.VerticalOffset) * TriggerChannel.YMultiplier;
                triggerCursors[ i ] = new Rectangle( gridClip.Right, cursorY - ScopeGlobal.IconMedium.Height / 2, clip.Right, cursorY + ScopeGlobal.IconMedium.Height / 2 );
            }
        }

        protected override void OnKeyDown( Key key )
        {
            base.OnKeyDown( key );

            if ( key == Key.Space )
            {
                CenterSampleWindow();
            }
        }

        protected override void OnMouseWheel( Mouse mouse )
        {
            base.OnMouseWheel( mouse );

            if ( Keyboard.IsDown( Key.Ctrl ) )
            {
                ActiveChannel.Channel.ChangeVerticalDivision( (mouse.WheelDelta > 0) ? -1 : 1 );
            }
            else
            {
                if ( ChangeTimeDiv( (mouse.WheelDelta > 0) ? -1 : 1 ) )
                {
                }
            }
        }

        protected override void OnMouseLeave( Mouse mouse )
        {
            base.OnMouseLeave( mouse );

            if ( !triggerCursorMoving )
                triggerCursorThatIsMoving = -1;

            if ( !channelCursorMoving )
                channelCursorThatIsMoving = -1;
        }

        protected override void OnMouseUp( Mouse mouse )
        {
            base.OnMouseUp( mouse );

            triggerCursorMoving = false;
            channelCursorMoving = false;
        }

        protected override void OnMouseDown( Mouse mouse )
        {
            base.OnMouseDown( mouse );

            if ( mouse.Button == MouseButton.Left )
            {
                mouseDownPosition = mouse.Position;
            }
        }

        protected override void OnMouseMove( Mouse mouse )
        {
            base.OnMouseMove( mouse );

            var yDistance = (mouseDownPosition.Y - mouse.Position.Y);
            var xDistance = (mouseDownPosition.X - mouse.Position.X);
            var mouseIsDown = mouse.Button == MouseButton.Left;

            if ( !triggerCursorMoving )
            {
                triggerCursorThatIsMoving = -1;

                for ( var i = 0; i < triggerCursors.Count; i++ )
                {
                    if ( triggerCursors[ i ].Contains( mouse.Position ) )
                    {
                        triggerCursorThatIsMoving = i;
                        triggerCursorMoving = mouseIsDown;
                    }
                }
            }

            if ( triggerCursorThatIsMoving != -1 )
            {
                if ( triggerCursorMoving )
                {
                    var distanceAsVolts = (yDistance / TriggerChannel.YMultiplier);
                    trigger[ triggerCursorThatIsMoving ] = (float)Math.Round( trigger[ triggerCursorThatIsMoving ] + distanceAsVolts, 2 );

                    mouseDownPosition = mouse.Position;
                }

                Update();
                return;
            }

            if ( !channelCursorMoving )
            {
                channelCursorThatIsMoving = -1;

                for ( var i = 0; i < channels.Count; i++ )
                {
                    if ( channels[ i ].Channel.Visible && channels[ i ].Cursor.Contains( mouse.Position ) )
                    {
                        channelCursorThatIsMoving = i;
                        channelCursorMoving = mouseIsDown;
                        channelOnTop = i;
                    }
                }
            }

            if ( channelCursorThatIsMoving != -1 )
            {
                if ( channelCursorMoving )
                {
                    var yDistanceAsPercent = (yDistance / gridClip.Height) * 1.0f;

                    if ( MoveY( yDistanceAsPercent ) )
                    {
                        mouseDownPosition = mouse.Position;
                    }
                }

                Update();
                return;
            }
        }

        protected override void OnLoad( Context context )
        {
            cursorLeftBitmap = new Bitmap( context, ScopeGlobal.Icons[ ScopeIcon.PinLeft ].Source );
            cursorRightBitmap = new Bitmap( context, ScopeGlobal.Icons[ ScopeIcon.PinRight ].Source );

            var cursorStrokeProps = new StrokeStyleProperties();
            cursorStrokeProps.StartCap = CapStyle.Flat;
            cursorStrokeProps.EndCap = CapStyle.Flat;
            cursorStrokeProps.DashCap = CapStyle.Flat;
            cursorStrokeProps.DashOffset = 0;
            cursorStrokeProps.DashStyle = DashStyle.Dash;
            cursorStrokeProps.MiterLimit = 1;
            cursorStrokeProps.LineJoin = LineJoin.Miter;
            cursorStroke = new StrokeStyle( cursorStrokeProps );
        }

        protected override void OnUpdate( Context context )
        {
            clip = new Rectangle( this.Area.Dimensions );
            gridClip = new Rectangle( clip.Left + 36f, clip.Top, clip.Right - 36f, clip.Bottom );

            var gridClipQuarter = new Rectangle( 0f, 0f, (float)Math.Floor( gridClip.Width / 2f ), (float)Math.Floor( gridClip.Height / 2f ) );
            var gridBitmapProperties = new BitmapProperties();
            gridBitmapProperties.PixelFormat = context.PixelFormat;
            gridBitmapProperties.DPIX = 96f;
            gridBitmapProperties.DPIY = 96f;
            gridBitmapProperties.BitmapOptions = BitmapOptions.Target;

            gridBitmap = new Bitmap( context, new Size( gridClipQuarter.Width, gridClipQuarter.Height ), gridBitmapProperties );
            gridBrush = new BitmapBrush( context, gridBitmap );
            gridBrush.ExtendModeX = ExtendMode.Mirror;
            gridBrush.ExtendModeY = ExtendMode.Mirror;
            gridBrush.Transform = Matrix3x2.Translation( gridClip.Location );

            using ( var gridGeometry = new PathGeometry() )
            {
                using ( var gridSink = gridGeometry.Open() )
                {
                    for ( var i = 0; i < (int)Math.Ceiling( gridClipQuarter.Height / gridSize.Height ); i++ )
                    {
                        gridSink.BeginFigure( new Point( gridClipQuarter.Left, gridClipQuarter.Bottom - (i * gridSize.Height) ), FigureBegin.Hollow );
                        gridSink.AddLine( new Point( gridClipQuarter.Right, gridClipQuarter.Bottom - (i * gridSize.Height) ) );
                        gridSink.EndFigure( FigureEnd.Open );
                    }

                    for ( var i = 0; i < (int)Math.Ceiling( gridClipQuarter.Width / gridSize.Width ); i++ )
                    {
                        gridSink.BeginFigure( new Point( gridClipQuarter.Right - (i * gridSize.Width), gridClipQuarter.Top ), FigureBegin.Hollow );
                        gridSink.AddLine( new Point( gridClipQuarter.Right - (i * gridSize.Width), gridClipQuarter.Bottom ) );
                        gridSink.EndFigure( FigureEnd.Open );
                    }

                    gridSink.Close();
                }

                //
                // Vaihdetaan ruudukko targettiin.
                //
                var originalTarget = context.GetTarget();
                context.SetTarget( gridBitmap );
                context.SetTransform( Matrix3x2.Identity );
                context.AntialiasMode = AntialiasMode.Aliased;

                context.BeginDraw();

                // Fill the background.
                var drawClip = new Rectangle( gridClip.Dimensions );
                SharedStyle.Align( drawClip );
                context.FillRectangle( drawClip, SharedStyle.Background );

                // Draw the grid and center lines.
                SharedBrush.Color = Color.Gainsboro;
                SharedBrush.Opacity = 0.1f;
                context.DrawGeometry( gridGeometry, SharedBrush, 1f );
                context.DrawLine( gridClipQuarter.BottomLeft, gridClipQuarter.BottomRight, SharedBrush, 1f );
                context.DrawLine( gridClipQuarter.BottomRight, gridClipQuarter.TopRight, SharedBrush, 1f );

                context.EndDrawNoPresent();

                //
                // Vaihdetaan takaisin normaaliin targettiin.
                //
                context.SetTarget( originalTarget );
                context.SetTransform( Transform );
            }

            UpdateTimeDiv();
            UpdatePoints();
        }

        protected override void OnDraw( Context context )
        {
            SharedStyle.Align( clip );
            context.FillRectangle( clip, SharedStyle.Background );

            context.AntialiasMode = AntialiasMode.PerPrimitive;
            context.FillRectangle( gridClip, gridBrush );

            SharedBrush.Opacity = 1f;

            for ( var i = 0; i < channels.Count; i++ )
            {
                var channel = channels[ i ];

                if ( channel.Channel.Visible )
                {
                    var cursorY = channel.Cursor.Top + channel.Cursor.Height / 2f;
                    var cursorIconRect = new Rectangle( ScopeGlobal.IconMedium ).CenterTo( channel.Cursor );

                    SharedBrush.Color = channel.Channel.Color;

                    context.AntialiasMode = AntialiasMode.Aliased;
                    context.FillOpacityMask( cursorRightBitmap, SharedBrush, cursorIconRect );
                    context.AntialiasMode = AntialiasMode.PerPrimitive;

                    if ( channelCursorThatIsMoving == i )
                    {
                        context.DrawLines( channel.Points, 0, samplesOnGrid, SharedBrush, 2f );

                        SharedBrush.Color = Color.WhiteSmoke;

                        var cursorTextRect = cursorIconRect;
                        cursorTextRect = cursorTextRect.Translate( 80f, -5f );
                        cursorTextRect = cursorTextRect.AdjustDimensions( 40f, 0f );

                        SharedText.Align( TextAlignment.Center, TextAlignment.Leading );
                        context.DrawText( ScopeUtil.FormatVolts( channel.Channel.VerticalOffset ), SharedText.Big, cursorTextRect, SharedBrush );
                        context.DrawLine( new Point( gridClip.Left, cursorY ), new Point( gridClip.Right, cursorY ), SharedBrush, 1f, cursorStroke );
                    }
                    else
                    {
                        context.DrawLines( channel.Points, 0, samplesOnGrid, SharedBrush, 1f );
                    }
                }
            }

            SharedBrush.Color = Color.WhiteSmoke;

            for ( var i = 0; i < triggerCursors.Count; i++ )
            {
                var cursorRect = triggerCursors[ i ];
                var cursorIconRect = new Rectangle( ScopeGlobal.IconMedium ).CenterTo( cursorRect );

                if ( triggerCursorThatIsMoving == i )
                {
                    var cursorY = cursorRect.Top + cursorRect.Height / 2f;
                    var cursorTextRect = cursorIconRect;
                    cursorTextRect = cursorTextRect.Translate( -80f, -5f );
                    cursorTextRect = cursorTextRect.AdjustDimensions( 40f, 0f );
                    context.DrawLine( new Point( gridClip.Left, cursorY ), new Point( gridClip.Right, cursorY ), SharedBrush, 1f, cursorStroke );
                    SharedText.Align( TextAlignment.Center, TextAlignment.Leading );
                    context.DrawText( ScopeUtil.FormatVolts( trigger[ 0 ] ), SharedText.Big, cursorTextRect, SharedBrush );
                }

                SharedBrush.Color = Color.WhiteSmoke;
                context.AntialiasMode = AntialiasMode.Aliased;
                context.FillOpacityMask( cursorLeftBitmap, SharedBrush, cursorIconRect );
                context.AntialiasMode = AntialiasMode.PerPrimitive;
            }

            SharedBrush.Color = Color.Black;
            context.DrawRectangle( clip, SharedBrush, 2f );
            context.DrawRectangle( gridClip, SharedBrush, 2f );
        }
    }
}

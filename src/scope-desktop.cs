﻿using System;
using System.Collections.Generic;
using System.Linq;

using Kodo.Graphics;
using Kodo.Graphics.Style;

namespace ScopeApp
{
    public class ScopeMainWindow : StyleWindow
    {
        Timer updateTimer;

        ScopeController controller;
        ScopeTrigger trigger;
        ScopeChannel channelOne;
        ScopeChannel channelTwo;

        HardwareSource hardwareSource;
        SoftwareSource softwareSource;

        ScopeView scopeView;
        ScopeChannelView channelView;
        ScopeControlView controlView;

        public ScopeMainWindow( WindowManager manager, WindowSettings settings )
            : base( manager, settings )
        {
            scopeView = new ScopeView( this );
            channelView = new ScopeChannelView( this );
            controlView = new ScopeControlView( this );

            updateTimer = new Timer( OnUpdate );
            updateTimer.Interval = 10;
        }

        void OnUpdate()
        {
            //
            // HÄRÖ HÄRÖ HÄRÖ
            //
            // Täytyy siirtää kaikki säädöt pois näyttö komponenteilta!
            //
            controller.TimeDivision = scopeView.SecondsPerDivision;

            controller.Update();
            channelView.Update();
            scopeView.Update();
            controlView.Update();
        }

        protected override void OnCreated()
        {
            base.OnCreated();

            trigger = new EdgeTrigger( EdgeType.Rising, -5.0 );

            hardwareSource = new HardwareSource( HardwareSource.AvailablePorts.FirstOrDefault() );
            hardwareSource.Start();
            softwareSource = new SoftwareSource( 1.5, 0, 1000, 1.0 / hardwareSource.Samplerate, 0.00 );
            softwareSource.Start();

            channelOne = new ScopeChannel( "CH 1", hardwareSource );
            channelOne.Color = Color.LightSkyBlue;
            channelOne.Visible = true;
            channelTwo = new ScopeChannel( "CH 2", softwareSource );
            channelTwo.Color = Color.IndianRed;
            channelTwo.Visible = false;

            controller = new ScopeController();
            controller.AddChannel( channelOne );
            controller.AddChannel( channelTwo );
            controller.SetTrigger( trigger );
            controller.SetTriggerChannel( channelOne );

            controlView.Controller = controller;
            channelView.AddChannel( channelOne );
            channelView.AddChannel( channelTwo );
            scopeView.AddChannel( channelOne );
            scopeView.AddChannel( channelTwo );
            scopeView.SetTrigger( trigger );
            scopeView.SetTriggerChannel( channelOne );

            updateTimer.Start();
        }

        protected override void OnUpdate( Context context )
        {
            base.OnUpdate( context );

            var client = this.Client;

            scopeView.Area = new Rectangle( client.Left, client.Top + 36f, client.Right, client.Bottom - 36f );
            channelView.Area = new Rectangle( client.Left, client.Bottom - 36f, client.Right, client.Bottom );
            controlView.Area = new Rectangle( client.Left, client.Top, client.Right, client.Top + 36f );

            controller.SetWindow( new ScopeWindow( scopeView.Area.Width / scopeView.GridSize.Width, scopeView.Area.Height / scopeView.GridSize.Height ) );
        }
    }
}
